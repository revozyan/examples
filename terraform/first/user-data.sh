#!/bin/bash
apt update -y
apt install nginx -y
myip=`curl http://169.254.169.254/latest/meta-data/local-ipv4`
echo "<h2>Webserver IP: $myip</h2><br>BUILD by Terraform" > /var/www/html/index.html
sudo systemctl start nginx
sudo systemctl enable nginx
