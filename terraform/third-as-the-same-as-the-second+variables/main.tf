### select region
provider "aws" {
  region = "us-east-1"
}

### get infomation about availability zones
data "aws_availability_zones" "available" {}

### get subnet's name
resource "aws_default_subnet" "default_az1" {
  availability_zone = data.aws_availability_zones.available.names[0]
}

### get subnet's name
resource "aws_default_subnet" "default_az2" {
  availability_zone = data.aws_availability_zones.available.names[1]
}

### select the latest ami ubuntu
data "aws_ami" "latest_ami_ubuntu" {
  owners      = ["099720109477"]
  most_recent = var.most_recent
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

### create security group which is gonna use for luach configuration
resource "aws_security_group" "security_group1" {
  name_prefix = "web-$(aws_security_group.security_group1)"

  dynamic "ingress" {
    for_each = var.allow_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name  = "dynamic security group"
    Onwer = "admin1"
  }
}

### create launch configuration
resource "aws_launch_configuration" "web_launch_configuration1" {
  name_prefix     = "web-$(aws_launch_configuration.web_launch_configuration1)"
  image_id        = data.aws_ami.latest_ami_ubuntu.id
  instance_type   = var.instance_type
  security_groups = [aws_security_group.security_group1.id]
  user_data       = file("/home/admin1/3-lesson/user_data.sh")
  lifecycle {
    create_before_destroy = true
  }
}

### create autoscaling group
resource "aws_autoscaling_group" "web_autoscaling_group1" {
  name_prefix          = "web-$(aws_autoscaling_group.web_autoscaling_group1)"
  launch_configuration = aws_launch_configuration.web_launch_configuration1.name
  min_size             = 2
  max_size             = 2
  min_elb_capacity     = 2
  vpc_zone_identifier  = [aws_default_subnet.default_az1.id, aws_default_subnet.default_az2.id]
  health_check_type    = "ELB"
  load_balancers       = [aws_elb.aws_elb1.name]

  dynamic "tag" {
    for_each = {
      Name   = "web-server"
      Owner  = "admin1"
      TAGKEY = "TAGVALUE"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
  lifecycle {
    create_before_destroy = true
  }
}

### create elastic load balancer
resource "aws_elb" "aws_elb1" {
  name               = "web-elb1"
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  security_groups    = [aws_security_group.security_group1.id]
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 80
    instance_protocol = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 10
  }
  tags = {
    Name = "web_elastic_load_balancer"
  }
}

### create a txt file for terraform's start/stop logs
resource "null_resource" "start" {
  provisioner "local-exec" {
    command = "echo Terraform START: $(date) >> log.txt"
  }
  depends_on = [aws_autoscaling_group.web_autoscaling_group1, aws_launch_configuration.web_launch_configuration1, aws_elb.aws_elb1]
}
