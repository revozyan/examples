#!/bin/bash
sudo apt update -y
sudo apt install nginx -y

myip=`curl http://169.254.169.254/latest/meta-data/local-ipv4`

echo "<h2>WebServer IP: $myip</h2><br>BUILD by Terraform - version 2" > /var/www/html/index.html
sudo systemctl start nginx
sudo systemctl enable nginx
