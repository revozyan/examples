groups:
### TARGETS ALERTS ##############################################################################################################################################################
- name: exporters_are_not_reachable_for_10_seconds
  rules:
  - alert: ExporterDown
    expr: up == 0
    for: 10s
    labels:
      severity: critical
    annotations:
      description: 'Metrics exporter service for {{ $labels.job }} running on {{ $labels.instance }} has been down for more than 10 seconds.'
      summary: 'Exporter down (instance {{ $labels.instance }})'


### *NIX ALERTS ##################################################################################################################################################################
- name: cpu_loading_more_than_70_percent
  rules:
  - alert: CpuLoad
    expr: 100 - (avg by(instance) (irate(node_cpu_seconds_total{mode="idle"}[5m])) * 100) > 70
    for: 10s
    labels:
      severity: critical
    annotations:
      summary: "High CPU load (instance {{ $labels.instance }})"
      description: "CPU load is high\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}"

  - alert: Host_Out_Of_Memory_less_than_10_percent_free
    expr: node_memory_MemAvailable_bytes / node_memory_MemTotal_bytes * 100 < 10
    for: 10s
    labels:
      severity: critical
    annotations:
      summary: "Host out of memory (instance {{ $labels.instance }})"
      description: "Node memory is filling up (< 10% left)\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}"

  - alert: Host_Out_Of_Disk_Space_less_than_10_percent
    expr: (node_filesystem_avail_bytes * 100) / node_filesystem_size_bytes < 10 and ON (instance, device, mountpoint) node_filesystem_readonly == 0
    for: 2m
    labels:
      severity: warning
    annotations:
      summary: "Host out of disk space (instance {{ $labels.instance }})"
      description: "Disk is almost full (< 10% left)\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}"


### WINDOWS ALERTS ##################################################################################################################################################################
  - alert: Windows_Host_Cpu_Loading_more_than_80_percent
    expr: 100 - (avg by (instance) (irate(wmi_cpu_time_total {job="prometheus-windows",mode="idle"}[2m])) * 100) > 80
    for: 10s
    labels:
      severity: critical
    annotations:
      summary: "CPU Usage (instance {{ $labels.instance }})"
      description: "CPU Usage is more than 80% VALUE = {{ $value }} LABELS: {{ $labels }}"

  - alert: Windows_Host_Out_Of_Memory_less_than_10_percent
    expr: wmi_os_physical_memory_free_bytes{job="prometheus-windows"} / wmi_cs_physical_memory_bytes{job="prometheus-windows"} < 0.1
    for: 10s
    labels:
      severity: critical
    annotations:
      summary: "Windows Host out of memory (instance {{ $labels.instance }})"
      description: "Node memory is filling up (< 10% left) VALUE = {{ $value }} LABELS: {{ $labels }}"

  - alert: Disk_Space_Free_10_Percent
    expr: node_filesystem_free_percent <= 10
    for: 5m
    labels:
      severity: warning
    annotations:
      summary: "Instance [{{ $labels.instance }}] has 10% or less Free disk space"
      description: "[{{ $labels.instance }}] has only {{ $value }}% or less free."


### Windows Services #############################################################################################################################################################
  - alert: WindowsServerCollectorError
    expr: windows_exporter_collector_success == 0
    for: 0m
    labels:
      severity: critical
    annotations:
      summary: "Windows Server collector Error (instance {{ $labels.instance }})"
      description: "Collector {{ $labels.collector }} was not successful\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}"
  - alert: Windows_IIS
    expr:  wmi_service_state{name="w3svc",state="running"} == 0
    for: 10s
    labels:
      severity: critical
    annotations:
      summary: "Windows Server service Status (instance {{ $labels.instance }})"
      description: "Windows Service state is not OK\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}\n for 10 seconds"
  - alert: Veeam_BackUP_Service
    expr: wmi_service_state{name="veeambackupsvc",state="running"} == 0
    for: 10s
    labels:
      severity: critical
    annotations:
      summary: "Veeam BackUP service is not running (instance {{ $labels.instance }})"
      description: "Windows Service state is not OK\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}\n for 10 seconds"
  - alert: EraServerSvc_Service
    expr:  wmi_service_state{name="eraserversvc",state="running"} == 0
    for: 10s
    labels:
      severity: critical
    annotations:
      summary: "ESET service is not running {{ $labels.instance }})"
      description: "Windows Service state is not OK\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}\n for 10 seconds"


### *nix Services ###################################################################################################################################################################
  - alert: Systemd servive does not running
    expr: node_systemd_unit_state{name="cron.service",state="active",type="simple"} == 0
    for: 10s
    labels:
      severity: critical
    annotations:
      summary: "CRON Service status (instance {{ $labels.instance }})"
      description: "Systemd's service - cron is not OK for 10 seconds\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}"


### VMWARER ALERTS ###
  - alert: Virtual_Machine_Memory_Warning_more_than_80
    expr: vmware_vm_mem_usage_average / 100 >= 80 and vmware_vm_mem_usage_average / 100 < 90
    for: 20s
    labels:
      severity: warning
    annotations:
      summary: "Virtual Machine Memory Warning (instance {{ $labels.instance }})"
      description: 'High memory usage on {{ $labels.instance }}: {{ $value | printf "%.2f"}}%\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}'
  - alert: Virtual_Machine_Memory_Critical_more_than_90
    expr: vmware_vm_mem_usage_average / 100 >= 90
    for: 20s
    labels:
      severity: critical
    annotations:
      summary: 'Virtual Machine Memory Critical (instance {{ $labels.instance }})'
      description: 'High memory usage on {{ $labels.instance }}: {{ $value | printf "%.2f"}}%\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}'
  - alert: High_Number_Of_Snapshots_more_than_2
    expr: vmware_vm_snapshots > 2
    for: 3m
    labels:
      severity: warning
    annotations:
      summary: 'High Number of Snapshots (instance {{ $labels.instance }})'
      description: 'High snapshots number on {{ $labels.instance }}: {{ $value }}\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}'

### CONTAINER ALERTS ###
  - alert: ContainerKilled
    expr: time() - container_last_seen > 60
    for: 0m
    labels:
      severity: warning
    annotations:
      summary: Container killed (instance {{ $labels.instance }})
      description: A container has disappeared\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}
  - alert: ContainerCpuUsage
    expr: (sum(rate(container_cpu_usage_seconds_total[3m])) BY (instance, name) * 100) > 80
    for: 2m
    labels:
      severity: warning
    annotations:
      summary: Container CPU usage (instance {{ $labels.instance }})
      description: Container CPU usage is above 80%\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}
  - alert: ContainerMemoryUsage
    expr: (sum(container_memory_working_set_bytes) BY (instance, name) / sum(container_spec_memory_limit_bytes > 0) BY (instance, name) * 100) > 80
    for: 2m
    labels:
      severity: warning
    annotations:
      summary: Container Memory usage (instance {{ $labels.instance }})
      description: Container Memory usage is above 80%\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}
  - alert: ContainerVolumeUsage
    expr: (1 - (sum(container_fs_inodes_free) BY (instance) / sum(container_fs_inodes_total) BY (instance))) * 100 > 80
    for: 2m
    labels:
      severity: warning
    annotations:
      summary: Container Volume usage (instance {{ $labels.instance }})
      description: Container Volume usage is above 80%\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}
  - alert: ContainerVolumeIoUsage
    expr: (sum(container_fs_io_current) BY (instance, name) * 100) > 80
    for: 2m
    labels:
      severity: warning
    annotations:
      summary: Container Volume IO usage (instance {{ $labels.instance }})
      description: Container Volume IO usage is above 80%\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}
  - alert: ContainerHighThrottleRate
    expr: rate(container_cpu_cfs_throttled_seconds_total[3m]) > 1
    for: 2m
    labels:
      severity: warning
    annotations:
      summary: Container high throttle rate (instance {{ $labels.instance }})
      description: Container is being throttled\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}
