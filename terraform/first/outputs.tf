output "server1_instance_id" {
  value = aws_instance.server1[*].id
}
output "server1_public_ip_address" {
  value = aws_eip.server1_static_ip[*].public_ip
}
output "server1_security_group_id" {
  value = aws_security_group.server1_security_group1.id
}
output "server1_private_ip_address" {
  value = aws_instance.server1[*].private_ip
}


output "server2_instance_id" {
  value = aws_instance.server2[*].id
}
output "server2_security_group_id" {
  value = aws_security_group.server2_security_group2.id
}
output "server2_private_ip_address" {
  value = aws_instance.server2[*].private_ip
}


output "server3_instance_id" {
  value = aws_instance.server3[*].id
}
output "server3_security_group_id" {
  value = aws_security_group.server3_security_group3.id
}
output "server3_private_ip_address" {
  value = aws_instance.server3[*].private_ip
}
