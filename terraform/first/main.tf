### regions
provider "aws" {
  region = "us-east-1"
}

### frontend servers
resource "aws_instance" "server1" {
  ami                    = "ami-09e67e426f25ce0d7"
  instance_type          = "t2.micro"
  count                  = 1
  user_data              = file("/home/ubuntu/user-data.sh")
  vpc_security_group_ids = [aws_security_group.server1_security_group1.id]
  tags = {
    Name    = "server1"
    Owner   = "admin1"
    Project = "first"
  }
  lifecycle {
    create_before_destroy = true
  }
  depends_on = [aws_instance.server2]
}

### backend servers
resource "aws_instance" "server2" {
  ami                    = "ami-09e67e426f25ce0d7"
  instance_type          = "t2.small"
  count                  = 1
  vpc_security_group_ids = [aws_security_group.server2_security_group2.id]
  tags = {
    Name    = "server2"
    Owner   = "admin1"
    Project = "first"
  }
  lifecycle {
    create_before_destroy = true
  }
  depends_on = [aws_instance.server3]
}

### database servers
resource "aws_instance" "server3" {
  ami                    = "ami-09e67e426f25ce0d7"
  instance_type          = "t2.small"
  count                  = 1
  vpc_security_group_ids = [aws_security_group.server3_security_group3.id]
  tags = {
    Name    = "server3"
    Owner   = "admin1"
    Project = "first"
  }
  lifecycle {
    create_before_destroy = true
  }
}

### elastic ip for the first frontend server
resource "aws_eip" "server1_static_ip" {
  instance = aws_instance.server1[0].id
}

### security_group for the frontend servers
resource "aws_security_group" "server1_security_group1" {
  name        = "security_group1"
  description = "Allow http port and others"
  dynamic "ingress" {
    for_each = ["80", "443", "22"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

### security_group for the backend servers
resource "aws_security_group" "server2_security_group2" {
  name        = "security_group2"
  description = "Allow ports for the backend servers"
  dynamic "ingress" {
    for_each = ["22", "4444"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

### security_group for the database servers
resource "aws_security_group" "server3_security_group3" {
  name        = "security_group3"
  description = "Allow ports for the datbase_servers"
  dynamic "ingress" {
    for_each = ["22", "5432"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
