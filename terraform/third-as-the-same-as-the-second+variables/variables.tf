### variable for instance type in launch configuration - string
variable "instance_type" {
  description = "select ami"
  type        = string
  default     = "t2.micro"
}

### variable for security group's ports - list
variable "allow_ports" {
  description = "specific allowed ports"
  default     = ["80", "443"]
}

### variable for create_before_destroy lifecycle
variable "most_recent" {
  description = "select true or false"
  type        = bool
  default     = "true"
}
